<?php

namespace App\Tests\Command;

use App\Command\GenerateSiteMapCommand;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;
use PHPUnit\Framework\TestCase;

class GenerateSiteMapCommandTest extends TestCase
{
  public function testLocaleNotSupported()
  {
    $application = new Application();

    $application->add(new GenerateSiteMapCommand());

    $command = $application->find('generate-sitemap');
    $commandTester = new CommandTester($command);
    $commandTester->execute(array(
      'command'  => $command->getName(),
      'locale' => 'unsupported',
    ));

    $output = $commandTester->getDisplay();
    $this->assertContains('Locale unsupported not supported, please use one of es-ES, fr-FR, it-IT', $output);
  }
}
