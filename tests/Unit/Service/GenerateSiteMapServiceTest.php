<?php

namespace App\Tests\Command;

use App\Api\MusementApi;
use App\Api\S3Api;
use App\Service\GenerateSiteMapService;
use App\Entity\City;
use App\Entity\Activity;
use App\Entity\Sitemap;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Application;

class GenerateSiteMapServiceTest extends TestCase
{
  protected $musementApi;
  protected $s3Api;
  protected $output;
  protected $locale;
  protected $sitemap;
  protected $service;

  protected function setUp()
  {
    $this->musementApi = $this->createMock(MusementApi::class);
    $this->s3Api = $this->createMock(S3Api::class);
    $this->output = $this->createMock(OutputInterface::class);
    $this->locale = 'a-locale';
    $this->service = new GenerateSiteMapService($this->musementApi, $this->s3Api);
    $this->sitemap = $this->createMock(Sitemap::class);
  }

  public function testRetrieveAllCities()
  {
    $city = new City(1, 'name', 'url');

    $this->musementApi->expects($this->once())->method('getAllCities')->willReturn([$city]);
    $this->musementApi->method('getAllActivitiesBy')->willReturn([]);
    $this->sitemap->expects($this->once())->method('add')->with($city);

    $this->service->execute($this->output, $this->sitemap, $this->locale);
  }

  public function testRetrieveActivitesByCities()
  {
    $city = new City(1, 'name', 'url');
    $activity = new Activity('name', 'url');
    $this->musementApi->method('getAllCities')->willReturn([$city]);

    $this->musementApi->expects($this->once())->method('getAllActivitiesBy')->willReturn([$activity]);
    $this->sitemap->expects($this->once())->method('addAll')->with([$activity]);

    $this->service->execute($this->output,  $this->sitemap, $this->locale);
  }

  public function testGenerateSitemap()
  {
    $this->musementApi->method('getAllCities')->willReturn([]);

    $this->sitemap->expects($this->once())->method('generateXML');

    $this->service->execute($this->output,  $this->sitemap, $this->locale);
  }

  public function testUploadSitemapToS3()
  {
    $this->musementApi->method('getAllCities')->willReturn([]);

    $this->s3Api->expects($this->once())->method('upload');

    $this->service->execute($this->output,  $this->sitemap, $this->locale);
  }
}
