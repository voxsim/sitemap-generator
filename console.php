#!/usr/bin/env php

<?php

require_once __DIR__ . '/vendor/autoload.php';

use Symfony\Component\Console\Application;

use App\Command\GenerateSiteMapCommand;

$app = new Application();
$app->add(new GenerateSiteMapCommand());
$app->run();
