<?php
namespace App\Service;

use App\Api\MusementApi;
use App\Api\S3Api;
use App\Entity\Sitemap;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateSiteMapService
{
  private $musementApi;
  private $s3Api;

  public function __construct(MusementApi $musementApi, S3Api $s3Api) {
    $this->musementApi = $musementApi;
    $this->s3Api = $s3Api;
  }

  public function execute(OutputInterface $output, Sitemap $sitemap, $locale)
  {
    $output->writeln('Download cities..');
    $cities = $this->musementApi->getAllCities();
    foreach($cities as $city) {
      $sitemap->add($city);
      $output->writeln("Download activities of {$city->getName()}..");
      $sitemap->addAll($this->musementApi->getAllActivitiesBy($city));
    }

    $output->writeln('Start upload..');
    $this->s3Api->upload("voxsim_{$locale}.xml", $sitemap->generateXML());
    $output->writeln('Upload Finished');
  }
}
