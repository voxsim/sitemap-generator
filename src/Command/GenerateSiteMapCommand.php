<?php
namespace App\Command;

use App\Api\MusementApi;
use App\Api\S3Api;
use App\Entity\Sitemap;
use App\Service\GenerateSiteMapService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateSiteMapCommand extends Command
{
  const supportedLocales = ['es-ES', 'fr-FR', 'it-IT'];

  protected function configure()
  {
    $this
      ->setName('generate-sitemap')
      ->setDescription('Creates a new sitemap.')
      ->setHelp('This command allows you to create the sitemap of musement')
      ->addArgument('locale', InputArgument::REQUIRED, 'You need to specify the locale\'s sitemap');
  }

  protected function execute(InputInterface $input, OutputInterface $output)
  {
    $locale = $input->getArgument('locale');
    if(!in_array($locale, self::supportedLocales)) {
      $output->writeln("Locale {$locale} not supported, please use one of " . join(self::supportedLocales, ', '));
      return;
    }

    $musementApi = MusementApi::use($locale);
    $s3 = S3Api::use($locale);
    $service = new GenerateSiteMapService($musementApi, $s3);

    $service->execute($output, Sitemap::create(), $locale);
  }
}
