<?php
namespace App\Normalizer;

use App\Entity\Sitemapable;
use Symfony\Component\Serializer\SerializerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class SitemapableNormalizer implements NormalizerInterface
{
  public function normalize($object, $format = null, array $context = array())
  {
    return [
      'loc' => $object->getUrl(),
      'priority' => $object->getPriority(),
    ];
  }

  public function supportsNormalization($data, $format = null)
  {
    return $data instanceof Sitemapable;
  }
}
