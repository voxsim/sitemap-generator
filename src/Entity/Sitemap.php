<?php
namespace App\Entity;

use App\Normalizer\SitemapableNormalizer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Serializer;

class Sitemap
{
  private $sitemapables;
  private $serializer;

  public function __construct(Serializer $serializer) {
    $this->serializer = $serializer;
    $this->sitemapables = [];
  }

  public function add(Sitemapable $sitemapable)
  {
    $this->sitemapables []= $sitemapable;
  }

  public function addAll(array $sitemapables)
  {
    foreach($sitemapables as $sitemapable)
    {
      $this->add($sitemapable);
    }
  }

  public function generateXML()
  {
    return $this->serializer->serialize(['url' => $this->sitemapables], 'xml', ['xml_format_output' => true]);
  }

  public static function create()
  {

    $encoders = array(new XmlEncoder('urlset'));
    $normalizers = array(new SitemapableNormalizer());

    $serializer = new Serializer($normalizers, $encoders);

    return new Sitemap($serializer);
  }
}
