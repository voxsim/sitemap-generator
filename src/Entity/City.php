<?php
namespace App\Entity;

class City implements Sitemapable
{
  private $id;
  private $name;
  private $url;

  public function __construct($id, $name, $url)
  {
    $this->id = $id;
    $this->name = $name;
    $this->url = $url;
  }

  public function getId()
  {
    return $this->id;
  }

  public function setId($id)
  {
    $this->id = $id;
  }

  public function getName()
  {
    return $this->name;
  }

  public function setName($name)
  {
    $this->name = $name;
  }

  public function getUrl()
  {
    return $this->url;
  }

  public function setUrl($url)
  {
    $this->url = $url;
  }

  public function getPriority()
  {
    return 0.7;
  }
}
