<?php
namespace App\Entity;

Interface Sitemapable
{
  public function getUrl();
  public function getPriority();
}
