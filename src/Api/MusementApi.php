<?php
namespace App\Api;

use App\Entity\City;
use GuzzleHttp\Client;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class MusementApi
{
  private $locale;
  private $client;
  private $serializer;

  function __construct($locale, Client $client, Serializer $serializer)
  {
    $this->locale = $locale;
    $this->client = $client;
    $this->serializer = $serializer;
  }

  public function getAllCities()
  {
    $response = $this->client->get('https://api.musement.com/api/v3/cities?limit=20', ['headers' => $this->headers()]);
    return $this->serializer->deserialize($response->getBody()->getContents(), 'App\Entity\City[]', 'json');
  }

  public function getAllActivitiesBy(City $city)
  {
    $response = $this->client->get("https://sandbox.musement.com/api/v3/cities/{$city->getId()}/activities?limit=20", ['headers' => $this->headers()]);
    return $this->serializer->deserialize($response->getBody()->getContents(), 'App\Entity\Activity[]', 'json');
  }

  private function headers() {
    return ['Accept-Language' => $this->locale];
  }

  public static function use($locale)
  {
    return new MusementApi($locale, new Client(), self::createSerializer());
  }

  private static function createSerializer()
  {
    $encoders = array(new JsonEncoder(), new XMLEncoder());
    $normalizers = array(new ObjectNormalizer(), new ArrayDenormalizer());
    return new Serializer($normalizers, $encoders);
  }
}
