<?php
namespace App\Api;

use Aws\S3\S3Client;

class S3Api
{
  private $locale;
  private $client;

  function __construct($locale, S3Client $client)
  {
    $this->locale = $locale;
    $this->client = $client;
  }

  public function upload($name, $content)
  {
    try {
      $this->client->putObject([
        'Bucket' => 'test-candidati',
        'Key'    => $name,
        'Body'   => $content,
        'ACL'    => 'public-read',
      ]);
    } catch (Aws\S3\Exception\S3Exception $e) {
      echo "There was an error uploading the file.\n";
    }
  }

  public static function use($locale)
  {
    $s3 = new S3Client([
      'version' => 'latest',
      'region'  => 'eu-west-1'
    ]);

    return new S3Api($locale, $s3);
  }
}
