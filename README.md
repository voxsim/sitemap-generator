# Musement Generator sitemap

## To install
`composer install`

Remember the tool requires php 7.1.3
Remember to set AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY environment variables before use it, like this:

```
export AWS_ACCESS_KEY_ID=<my access key>
export AWS_SECRET_ACCESS_KEY=<my secret key>
```

## To launch tests
`./vendor/bin/phpunit`

## To generate the sitemap
`php console.php generate-sitemap it-IT`

## To be production ready, we need:
- Test the integration between musement api and the tool (http://docs.guzzlephp.org/en/stable/testing.html)
- Create a docker machine
